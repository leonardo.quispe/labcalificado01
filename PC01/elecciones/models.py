from django.db import models

# Create your models here.
class Region(models.Model):
    region_texto = models.CharField(max_length=200)

class Candidato(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    candidato_texto = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)