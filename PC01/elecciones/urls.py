from django.urls import path
from . import views

app_name = 'elecciones'

urlpatterns = [
    #ex: /elecciones/
    path('', views.index, name='index'),
    #ex: /elecciones/5/
    path('<int:region_id>/', views.detalle, name='detalle'),
    #ex: /elecciones/5/resultados/
    path('<int:region_id>/resultados/', views.resultados, name='resultados'),
    #ex: /elecciones/5/voto/
    path('<int:region_id>/voto/', views.votar, name='votar'),
]